using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SDA.Utilis
{
    public interface IPoolable
    {
        void PrepareForActivate();
        void PrepareForDeactivate(Transform parent);
    } 
}
