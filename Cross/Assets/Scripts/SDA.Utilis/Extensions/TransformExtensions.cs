using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SDA.Utilis
{
    public static class TransformExtensions // dzia�a tylko w MonoBehavior
    {
        public static List<Transform> GetChildren(this Transform transform)
        {
            var children = new List<Transform>();

            foreach (Transform child in transform)
            {
                children.Add(child);
            }
            return children;
        }
    }
}
