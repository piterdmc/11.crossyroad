using DG.Tweening;
using SDA.Loop;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SDA.UI
{
    public class LoseView : BaseView
    {
        [SerializeField]
        private Button restartButton;

        public Button RestartButton => restartButton;

        [SerializeField]
        private Text scorePoints;
        [SerializeField]
        private Text bestScore;

        public void UpdatePoints(int points)
        {
            
            StartCoroutine(CountPoints(points));

        }


        private IEnumerator CountPoints(int points) 
        {
            for (int i = 0; i < points; ++i)
            {
                this.scorePoints.text = $"points: {i}";
                yield return new WaitForSeconds(1f/points);
               
            }

            var sequence = DOTween.Sequence();
            sequence.Append(scorePoints.transform.DOScale(1.2f, 0.2f))
            .Append(scorePoints.transform.DOScale(1f, 0.2f))
            .SetEase(Ease.Linear);
           
        }
    } 
}
