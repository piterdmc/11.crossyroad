using SDA.Loop;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SDA.UI
{
    public class GameView : BaseView
    {
        [SerializeField]
        private Text score; 

        [SerializeField]
        private Text bestScore;

        public void UpdateScore(int score, int bestScore) 
        {
            this.score.text = score.ToString();
            this.bestScore.text = bestScore.ToString();
        }
    } 
}
