using SDA.Data;
using SDA.Loop;
using SDA.Utilis;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace SDA.Generation
{
    [Serializable]
    public class Chunk
    {
        public Vector2 ranges;
        public List<LaneTemplate> templates;
        public bool IsBetween(int count)
        {
            if ((int)ranges.x <= count && count < (int)ranges.y)
            {
                return true;
            }
            return false;
        }
    }


    public class LaneGenerator : MonoBehaviour
    {
        [SerializeField]
        private Transform lanesParent;
        [SerializeField]
        private Transform startingPos;
        [SerializeField]
        private float distance = 1.5f;

        private CarPool<BaseCar> carPool;
        private int counter;

        private LaneTemplate currentTemplate;
        private int templateIterator = 0;



        [SerializeField]
        private List<Chunk> chunks;
        public void GenerateLevel(CarPool<BaseCar> carPool, int lanesCount)
        {
            this.carPool = carPool;
            var chunk = GetChunk();
            currentTemplate = GetTemplate(chunk);
            
            for (int i = 0; i < lanesCount; ++i)
            {
                var randomCar = Random.Range(0, 9);
                var spawnPointIndex = Random.Range(0, 2);

                GenerateLane(carPool, (CarType)randomCar, spawnPointIndex);
            }
        }

        private void OnLaneDespawn()
        {
            var randomCar = Random.Range(0, 9);
            var spawnPointIndex = Random.Range(0, 2);
            GenerateLane(carPool, (CarType)randomCar, spawnPointIndex);
        }

        private Chunk GetChunk()
        {
            for (int i = 0; i < chunks.Count; ++i)
            {
                var chunk = chunks[i];
                if (chunk.IsBetween(counter))
                {
                    return chunk;
                }
            }
            return chunks[chunks.Count-1];

        }

        private LaneTemplate GetTemplate(Chunk chunk) 
        {
            var randomTemplate = Random.Range(0, chunk.templates.Count);
            return chunk.templates[randomTemplate];
        }


        public void GenerateLane(CarPool<BaseCar> carPool, CarType carType, int spawnPointIndex)
        {
            if (templateIterator == currentTemplate.lanes.Count)
            {
                var chunk = GetChunk();
                currentTemplate = GetTemplate(chunk);
                templateIterator = 0;
            }
            var lanePrefab = currentTemplate.lanes[templateIterator];
            templateIterator++;

            var lane = Instantiate(lanePrefab, lanesParent, true);
            lane.transform.position = startingPos.position + Vector3.right * distance * counter;
            lane.SetColor(counter);
            if (lane.overridingData.isOverriding)
            {
                carType = lane.overridingData.type;
            }
            lane.InitializeLane(carPool, carType, spawnPointIndex);
            lane.OnDespawnAddListener(OnLaneDespawn);
            counter++;



        }





    }
}
