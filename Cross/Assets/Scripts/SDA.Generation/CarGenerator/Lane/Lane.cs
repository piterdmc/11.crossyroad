using SDA.Data;
using SDA.Loop;
using SDA.Utilis;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SDA.Generation
{
    public class Lane : MonoBehaviour
    {
        [SerializeField]
        private CarGenerator carGenerator;
        [SerializeField]
        MeshRenderer meshRenderer;
       

        [SerializeField]
        Color brightColor;
        [SerializeField]
        Color darkColor;

        [SerializeField]
        private bool isOverride = false;
        [SerializeField]
        private CarType overridngCar;
        

        private UnityAction onDespawn;

        public (bool isOverriding, CarType type) overridingData => (isOverride, overridngCar);

        public void SetColor(int count)
        {
            if (count % 2 == 0)
            {
                meshRenderer.material.color = brightColor;
            }
            else
            {
                meshRenderer.material.color = darkColor;
            }
        }

        public void InitializeLane(CarPool<BaseCar>pool, CarType type, int spawnPointIndex ) 
        {
            carGenerator.InitializeGenerator(pool, type, spawnPointIndex);
            var time = Random.Range(2f, 10f);
            StartCoroutine(GenerateCar(time));
        }

        private IEnumerator GenerateCar(float timeBeetweenSpawns) 
        {
            while (true)
            {
                carGenerator.SpawnCar(transform.parent);
                yield return new WaitForSeconds(timeBeetweenSpawns);
            }
        }

        public void OnDespawnAddListener(UnityAction callback) 
        {
            onDespawn = callback;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Wall"))
            {
                StopAllCoroutines();
                carGenerator.DespawnCars();
                onDespawn.Invoke();
                Destroy(this.gameObject);
            }
        }


    }
}
