using SDA.Data;
using SDA.Loop;
using SDA.Utilis;
using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


namespace SDA.Generation
{
    public enum Direction
    {
        Forward,
        Backward
    }

    [Serializable]
    public class SpawnPoint
    {
    
        public Transform spawnPointTransform;
        public Direction direction;

        public Vector3 GetDirection() 
        {
            if (direction == Direction.Forward)
            {
                return Vector3.forward;
            }
            else
            {
                return Vector3.back;
            }
        }
    }

    public class CarGenerator : MonoBehaviour
    {
        [SerializeField]
        private SpawnPoint[] spawnPoint;

        private CarPool<BaseCar> pool;
        private CarType typeToSpawn;
        private int spawnPointIndex;
        private List<BaseCar> spawnedCars = new List<BaseCar>();
       
        [SerializeField]
        private bool isEnabled = true;

        public void InitializeGenerator(CarPool<BaseCar> pool, CarType type, int spawnIndex) 
        {
            this.pool = pool;
            typeToSpawn = type;
            spawnPointIndex = spawnIndex;
        }

        private void ReturnToPool(BaseCar car) 
        {
            spawnedCars.Remove(car);
            pool.ReturnToPool(typeToSpawn, car);
        }

        public void SpawnCar(Transform parent)
        {
            if (!isEnabled)
            {
                return;
            }
            var obj = pool.GetFromPool(typeToSpawn);
            obj.transform.SetParent(parent);

            obj.transform.position = spawnPoint[spawnPointIndex].spawnPointTransform.position;
            obj.transform.rotation = spawnPoint[spawnPointIndex].spawnPointTransform.rotation;
            obj.onWallHitAddListener(ReturnToPool);
            obj.StartMove(spawnPoint[spawnPointIndex].GetDirection(), obj.GetSpeed());
            spawnedCars.Add(obj);
            

        }
        public void DespawnCars() 
        {
            for (int i = 0; i < spawnedCars.Count; ++i)
            {
                pool.ReturnToPool(typeToSpawn, spawnedCars[i]);
            }
            spawnedCars.Clear();
        }
    }
}
