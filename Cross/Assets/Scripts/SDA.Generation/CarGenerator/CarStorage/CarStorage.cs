using SDA.Data;
using SDA.Loop;
using SDA.Utilis;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SDA.Generation
{
    public class CarStorage : MonoBehaviour
    {
        [SerializeField]
        private BaseCar ambulancePrefab;
        [SerializeField]
        private BaseCar deliveryPrefab;
        [SerializeField]
        private BaseCar fireTruckPrefab;
        [SerializeField]
        private BaseCar policePrefab;
        [SerializeField]
        private BaseCar racePrefab;
        [SerializeField]
        private BaseCar suvPrefab;
        [SerializeField]
        private BaseCar taxiPrefab;
        [SerializeField]
        private BaseCar tractorPrefab;
        [SerializeField]
        private BaseCar truckPrefab;
        [SerializeField]
        private BaseCar vanPrefab; 
        [SerializeField]
        private BaseCar trainPrefab;
      


        [SerializeField]
        private Transform storageParent;

        private CarPool<BaseCar> carsPool;

        public CarPool<BaseCar> CarsPool => carsPool;

       /* private void Start()
        {
            InitializeStorage();
        }*/

        public void InitializeStorage() 
        {
            var sizes = new Dictionary<CarType, int>();
            sizes.Add(CarType.Ambulance, 30);
            sizes.Add(CarType.Delivery, 30);
            sizes.Add(CarType.FireTruck, 30);
            sizes.Add(CarType.Police, 30);
            sizes.Add(CarType.Race, 30);
            sizes.Add(CarType.Suv, 30);
            sizes.Add(CarType.Taxi, 30);
            sizes.Add(CarType.Tractor, 30);
            sizes.Add(CarType.Truck, 30);
            sizes.Add(CarType.Van, 30);
            sizes.Add(CarType.Train, 10);

            carsPool = new CarPool<BaseCar>(sizes, storageParent);
            InitializePool(CarType.Ambulance, ambulancePrefab);
            InitializePool(CarType.Delivery, deliveryPrefab);
            InitializePool(CarType.FireTruck, fireTruckPrefab);
            InitializePool(CarType.Police, policePrefab);
            InitializePool(CarType.Race, racePrefab);
            InitializePool(CarType.Suv, suvPrefab);
            InitializePool(CarType.Taxi, taxiPrefab);
            InitializePool(CarType.Tractor, tractorPrefab);
            InitializePool(CarType.Truck, truckPrefab);
            InitializePool(CarType.Van, vanPrefab);
            InitializePool(CarType.Train, trainPrefab);


        }

        public void InitializePool(CarType type, BaseCar prefab) 
        {
            for (int i = 0; i < carsPool.GetSize(type); ++i)
            {
                var obj = Instantiate(prefab);
                carsPool.ReturnToPool(type, obj);
            }

        }


    } 
}
