using SDA.Loop;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SDA.Data
{
    [CreateAssetMenu(menuName = "Crossy/Car/New Car")]

    public class CarData : ScriptableObject
    {
       
        [SerializeField]
        public float baseSpeed;


        public float BaseSpeed
        {
            get
            {
                return baseSpeed;
            }
        }




    } 
}
