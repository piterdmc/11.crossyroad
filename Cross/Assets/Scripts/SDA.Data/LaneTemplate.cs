using SDA.Generation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SDA.Data
{
    [CreateAssetMenu(menuName = "Crossy/Lane/New Lane")]
    public class LaneTemplate : ScriptableObject
    {
        public List<Lane> lanes; 
        
    } 
}
