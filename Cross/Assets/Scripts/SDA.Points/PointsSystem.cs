using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SDA.Points
{
    public class PointsSystem
    {
        private int bestScore;

        public int BestScore 
        {
            get 
            {
                if (currentScore < bestScore)
                {
                    return bestScore;
                }
                return currentScore;
            }
        }

        private int currentScore;
        public int CurrentPoints => currentScore;

        private bool isBestScorAchived = false;

        UnityAction onBestScore;

        public void InitializeSystem(int bestScore) 
        {
           this.bestScore = bestScore;
        }
     
        public void AddPoints() 
        {

            currentScore++;
            if (currentScore > bestScore && !isBestScorAchived)
            {
                isBestScorAchived = true;
                onBestScore.Invoke();
            }

        }
        public void OnBestScoreAddListener(UnityAction callback)
        {
            onBestScore = callback;
        }
    } 
}
