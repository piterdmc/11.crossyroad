using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SDA.Loop
    //ctrl + k + s namespace
{    public abstract class BaseState
    {
        public abstract void InitState();
        public abstract void UpdateState();
        public abstract void DisposeState();


    }
}

