using SDA.Generation;
using SDA.Input;
using SDA.Player;
using SDA.Points;
using SDA.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SDA.Loop
{
    public class GameController : MonoBehaviour
    {
        #region STATES

        private MenuState menuState;
        private GameState gameState;
        private LoseState loseState;
        
        private PointsSystem pointsSystem;
        private SaveSystem saveSystem;

        #endregion

        private UnityAction toGameStateTransition;
        private UnityAction toMenuStateTransition;
        private UnityAction toLoseStateTransition;

        [SerializeField]
        private CrossInput crossyInput;
        private BaseState currentlyActiveState;
        [SerializeField]
        MenuView menuView;
        [SerializeField]
        GameView gameView;
        [SerializeField]
        private LoseView loseView;
        [SerializeField]
        private LaneGenerator laneGenerator;
        [SerializeField]
        private CarStorage carStorage;
        [SerializeField]
        private PlayerMovement playerMovement;
        [SerializeField]
        private CameraMovement cameraMovement;
        [SerializeField]
        private AudioSystem audioSystem;

        private void Start()
        {
            toGameStateTransition = () => ChangeState(gameState);
            toMenuStateTransition = () => ChangeState(menuState);
            toLoseStateTransition = () => ChangeState(loseState);


            pointsSystem = new PointsSystem();
            saveSystem = new SaveSystem();

            menuState = new MenuState(crossyInput, toGameStateTransition, menuView, laneGenerator, carStorage, pointsSystem, saveSystem, audioSystem);
            gameState = new GameState(crossyInput, gameView, cameraMovement, playerMovement, toLoseStateTransition, pointsSystem, audioSystem);
            loseState = new LoseState(loseView, pointsSystem, saveSystem);

            ChangeState(menuState);

        }
        private void Update()
        {
            currentlyActiveState?.UpdateState();
        }
        private void OnDestroy()
        {

        }

        private void ChangeState(BaseState newState)
        {
            currentlyActiveState?.DisposeState();
            currentlyActiveState = newState;
            newState.InitState();
        }

        private void OnApplicationQuit()
        {
            saveSystem.LoadedData.bestScore = pointsSystem.BestScore;
            saveSystem.SaveData();
        }

    }
}
