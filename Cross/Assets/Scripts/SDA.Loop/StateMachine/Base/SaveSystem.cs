using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SDA.Loop
{
    [Serializable]
    public class PlayerData
    {
        public int bestScore;

        public PlayerData() 
        {
            bestScore = 10;
        }

    }
    public class SaveSystem : MonoBehaviour
    {
        private PlayerData loadedData;
        public PlayerData LoadedData => loadedData;

        
        public void LoadData() 
        {
            if (PlayerPrefs.HasKey("CROSSY_SAVE1"))
            {
                var json = PlayerPrefs.GetString("CROSSY_SAVE1");
                loadedData = JsonUtility.FromJson<PlayerData>(json);
            }
            else
            {
                loadedData = new PlayerData();
                SaveData();
            }
        }

        public void SaveData()
        {
            var json = JsonUtility.ToJson(loadedData);

            PlayerPrefs.SetString("CROSSY_SAVE1", json);
        }
    }
}
