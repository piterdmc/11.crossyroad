using SDA.Data;
using SDA.Utilis;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SDA.Loop
{
    public class BaseCar : MonoBehaviour, IPoolable
    {
        [SerializeField]
        Rigidbody rb;

        [SerializeField]
        private CarData carData;

        private UnityAction<BaseCar> onWallHit;

        public void StartMove(Vector3 direction, float speed) 
        {
            rb.AddForce(direction * (speed * Time.fixedDeltaTime) , ForceMode.Impulse);
        }

        public float GetSpeed()
        {
            return carData.baseSpeed;
        }

        public void onWallHitAddListener(UnityAction<BaseCar> onWallHit)
        {
            this.onWallHit = onWallHit;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Wall"))
            {
                onWallHit.Invoke(this);
            }
        }

 
        public void PrepareForActivate()
        {
            rb.velocity = Vector3.zero;
            gameObject.SetActive(true);
        }

        public void PrepareForDeactivate(Transform parent)
        {
            gameObject.SetActive(false);
            transform.SetParent(parent);
        }

    } 
}
