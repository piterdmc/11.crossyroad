using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SDA.Loop
{
    public class AudioSystem : MonoBehaviour
    {
        [SerializeField]
        private AudioSource backgroundMusicAudioSource;

        [SerializeField]
        private AudioSource soundsAudioSorce;

        [SerializeField]
        private AudioClip menuMusic;
        [SerializeField]
        private AudioClip gameMusic;
        [SerializeField]
        private AudioClip deathMusic;
        [SerializeField]
        private AudioClip jumpMusic; 
        [SerializeField]
        private AudioClip bestScore;


        public void PlayMenuMusic() 
        {
            backgroundMusicAudioSource.Stop();
            backgroundMusicAudioSource.clip = menuMusic;
            backgroundMusicAudioSource.Play();
        }

        public void PlayGameMusic()
        {
            backgroundMusicAudioSource.Stop();
            backgroundMusicAudioSource.clip = gameMusic;
            backgroundMusicAudioSource.Play();
        }


        public void PlayDeathMusic()
        {

            soundsAudioSorce.PlayOneShot(deathMusic);
        }

        public void PlayJumpMusic()
        {

            soundsAudioSorce.PlayOneShot(jumpMusic);
        }

        public void PlayBestScoreMusic()
        {

            soundsAudioSorce.PlayOneShot(bestScore);
        }

    } 
}
