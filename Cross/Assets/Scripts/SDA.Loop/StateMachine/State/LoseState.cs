using SDA.Points;
using SDA.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace SDA.Loop
{
    public class LoseState : BaseState
    {
        private LoseView loseView;
        private PointsSystem pointsSystem;
        private SaveSystem saveSystem;

        

        public LoseState(LoseView loseView, PointsSystem pointsSystem, SaveSystem scoreSystem)
        {
            this.loseView = loseView;
            this.pointsSystem = pointsSystem;
            this.saveSystem = scoreSystem;
        }

        public override void InitState()
        {

            loseView.ShowView();
            saveSystem.LoadedData.bestScore = pointsSystem.BestScore;
            saveSystem.SaveData();

            loseView.UpdatePoints(pointsSystem.CurrentPoints);
            loseView.RestartButton.onClick.AddListener(RestartGame);
           
 
        }

        public override void UpdateState()
        {
            //loseView.UpdateScore(pointsSystem.CurrentPoints, pointsSystem.BestScore);
        }

        public override void DisposeState()
        {
            if (loseView != null)
            {
                loseView.HideView();
            }
            
        }

        public void RestartGame()
        {
            SceneManager.LoadScene(0);
        }
    }
}
