using SDA.Input;
using SDA.Player;
using SDA.Points;
using SDA.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace SDA.Loop
{
    public class GameState : BaseState
    {
        private CrossInput crossyInput;

        private GameView gameView;
        private CameraMovement cameraMovement;
        private PlayerMovement playerMovement;
        private PointsSystem pointsSystem;
        private AudioSystem audioSystem;



        private UnityAction transitionToLoseState;
       
        public GameState(CrossInput crossyInput, GameView gameView, CameraMovement cameraMovement, PlayerMovement playerMovement,
            UnityAction transitionToLoseState, PointsSystem pointsSystem, AudioSystem audioSystem)
        {
            this.crossyInput = crossyInput;
            this.gameView = gameView;
            this.cameraMovement = cameraMovement;
            this.playerMovement = playerMovement;
            this.transitionToLoseState = transitionToLoseState;
            this.pointsSystem = pointsSystem;
            this.audioSystem = audioSystem;
          
        }

        public override void InitState()
        {
            if (gameView != null)
            {
                gameView.ShowView();
                /*UnityAction onDieAction = transitionToLoseState;
                onDieAction += audioSystem.PlayDeathMusic;*/
                transitionToLoseState += audioSystem.PlayDeathMusic;

                playerMovement.OnDieAddListener(transitionToLoseState, audioSystem.PlayDeathMusic);
                playerMovement.OnJumpAddListener(audioSystem.PlayJumpMusic);
                pointsSystem.OnBestScoreAddListener(audioSystem.PlayBestScoreMusic);

                
                crossyInput.AddListener(InputType.Forward, playerMovement.MoveForward);
                crossyInput.AddListener(InputType.Backward, playerMovement.MoveBackward);
                crossyInput.AddListener(InputType.Left, playerMovement.MoveLeft);
                crossyInput.AddListener(InputType.Right, playerMovement.MoveRight);
                playerMovement.InitPlayer(pointsSystem);
                playerMovement.MoveForward();
                audioSystem.PlayGameMusic();

                
            }
           
        }

        public override void UpdateState()
        {
            gameView.UpdateScore(pointsSystem.CurrentPoints, pointsSystem.BestScore);
            cameraMovement.UpdateMovement();
            
        }

        public override void DisposeState()
        {
            crossyInput.ClearInputs();
            if (gameView != null)
            {
                gameView.HideView();
              
            }
        }

        public void IncreasePoints() 
        {
            pointsSystem.AddPoints();
            gameView.UpdateScore(pointsSystem.CurrentPoints, pointsSystem.BestScore);
        }

        


    }

}
