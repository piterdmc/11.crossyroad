using SDA.Generation;
using SDA.Input;
using SDA.Player;
using SDA.Points;
using SDA.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SDA.Loop
{
    public class MenuState : BaseState

    {
        private CrossInput crossyInput;
        public MenuView menuView;
        public LaneGenerator laneGenerator;
        public CarStorage carStorage;



        private UnityAction transitionToGameState; 
        private UnityAction transitionToMenuState;
        private PointsSystem pointsSystem;
        private SaveSystem saveSystem;
        private AudioSystem audioSystem;

        public MenuState(CrossInput crossyInput, UnityAction transitionToGameState, MenuView menuView, LaneGenerator laneGenerator, 
            CarStorage carStorage, PointsSystem pointsSystem, SaveSystem scoreSystem, AudioSystem audioSystem)
        {
            this.crossyInput = crossyInput;
            this.transitionToGameState = transitionToGameState;
            this.menuView = menuView;
            this.laneGenerator = laneGenerator;
            this.carStorage = carStorage;
            this.pointsSystem = pointsSystem;
            this.saveSystem = scoreSystem;
            this.audioSystem = audioSystem;
            
            
        }

        public override void InitState()
        {
            if (menuView != null)
            {
                saveSystem.LoadData();
                pointsSystem.InitializeSystem(saveSystem.LoadedData.bestScore);
                   
                menuView.ShowView();
                carStorage.InitializeStorage();
                crossyInput.AddListener(InputType.Space, transitionToGameState.Invoke);            
                laneGenerator.GenerateLevel(carStorage.CarsPool, 15);
                audioSystem.PlayMenuMusic();
                
            }
            
            
        }

        public override void UpdateState()
        {
            Debug.Log("UPDATE");
         
        }


        public override void DisposeState()
        {
            crossyInput.ClearInputs();
            if (menuView != null)
            {
                menuView.HideView();
                
            }
          
        }

        public void ToGameState() 
        {
            transitionToGameState.Invoke();
        }

    }
}
