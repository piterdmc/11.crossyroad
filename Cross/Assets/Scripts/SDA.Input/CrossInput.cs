using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace SDA.Input
{
    public enum InputType
    {//index inputu
        Forward,
        Backward,
        Left,
        Right,
        Space
    }

    public class CrossInput : MonoBehaviour
    {
        [SerializeField]
        private PlayerInput playerInput;
        private UnityAction onForward;
        private UnityAction onBackward;
        private UnityAction onLeft;
        private UnityAction onRight;
        private UnityAction onSpace;

        public void AddListener(InputType inputType, UnityAction callback)
        {
            switch (inputType)
            {
                case InputType.Forward:
                    onForward += callback;
                    break;
                case InputType.Backward:
                    onBackward += callback;
                    break;
                case InputType.Left:
                    onLeft += callback;
                    break;
                case InputType.Right:
                    onRight += callback;
                    break;
                case InputType.Space:
                        onSpace += callback;
                    break;
            }
        }

        public void ClearInputs()
        {
            onForward = null;
            onBackward = null;
            onLeft = null;
            onRight = null;
            onSpace = null;
        }
        public void OnMoveForward(InputAction.CallbackContext ctx)
        {

            if (ctx.action.WasPerformedThisFrame())
            {
                onForward?.Invoke();
            }
            


        }
        public void OnMoveBackward(InputAction.CallbackContext ctx)
        {
            if (ctx.action.WasPerformedThisFrame())
            {
                onBackward?.Invoke();
            }
         
        }
        public void OnMoveLeft(InputAction.CallbackContext ctx)
        {
            if (ctx.action.WasPerformedThisFrame())
            {
                onLeft?.Invoke();
            }
           
        }
        public void OnMoveRight(InputAction.CallbackContext ctx)
        {
            if (ctx.action.WasPerformedThisFrame())
            {
                onRight?.Invoke();
            }
         
        }
        public void OnSpacePress(InputAction.CallbackContext ctx)
        {
            if (ctx.action.WasPerformedThisFrame())
            {
                onSpace?.Invoke();
            }           
        }







    }
}
