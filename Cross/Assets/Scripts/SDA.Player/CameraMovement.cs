using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SDA.Player
{
    public class CameraMovement : MonoBehaviour
    {
        [SerializeField]
        private Transform chickenPrefab;
        [SerializeField]
        private Transform cameraPoint;

       public float speed = 10f;

        [SerializeField]
        private float catchupSpeed;

        [SerializeField]
        private float treshold;

        private bool duringCatching = false;
        public void UpdateMovement() 
        {
            var distance = chickenPrefab.position.x - cameraPoint.position.x;
            if (distance > treshold && !duringCatching)
            {
                transform.DOMove(transform.position + Vector3.right *  catchupSpeed, 1f).OnComplete(()=> duringCatching = false);
                duringCatching = true;
                return;
            }
            if (duringCatching)
            {
                return;
            }
          
            transform.position += Vector3.right * Time.deltaTime * speed;  
        }







      /*  public void CalculateDistance() 
        {
           
            if (ChickenPrefab)
            {
                float distanceBetweenCameraAndPlayer = Vector3.Distance(ChickenPrefab.position, CameraPoint.position);
                print("Distance to other: " + distanceBetweenCameraAndPlayer);
            }
            
        }*/
    } 
}
