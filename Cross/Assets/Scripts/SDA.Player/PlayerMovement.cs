using DG.Tweening;
using SDA.Points;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SDA.Player
{

    public class PlayerMovement : MonoBehaviour
    {
        private bool canMove = true;
        private float duration = 0.2f;
        private float skip = 1.5f;
        private UnityAction onDie;
        private UnityAction onDieSound;
        private UnityAction onJump;
        private Vector3 startPos;
        private float highestX;
        private PointsSystem pointsSystem;

        private bool isDead;

        [SerializeField]
        Transform deadScale;

        public void InitPlayer(PointsSystem pointsSystem)
        {
            this.pointsSystem = pointsSystem;
            highestX = transform.position.x;
            startPos = transform.position;
        }
        public void MoveForward()
        {
            if (!canMove)
            {
                return;
            }
            if (Physics.Raycast(this.transform.position, Vector3.right, 1.5f, 256))
                return;
            

            canMove = false;
            transform.DORotate(new Vector3(0, 0, 0), 0.2f);
            var endValue = transform.position + Vector3.right * skip;
            var endValueX = endValue.x;
            if (endValueX > highestX)
            {
                pointsSystem.AddPoints();
                highestX = endValueX;

            }

            transform.DOJump(endValue, 1f, 1, 0.2f).OnComplete(() => canMove = true);
            onJump.Invoke();

        }

        public void MoveBackward()
        {
            if (!canMove)
            {
                return;
            }
            if (Physics.Raycast(this.transform.position, Vector3.left, 1.5f, 256))
            {
                return;
            }

            canMove = false;
            transform.DORotate(new Vector3(0, -180, 0), 0.2f);
            transform.DOJump(transform.position + Vector3.left * skip, 1f, 1, 0.2f).OnComplete(() => canMove = true);
            onJump.Invoke();

        }
        public void MoveLeft()
        {
            if (!canMove)
            {
                return;
            }
            if (Physics.Raycast(this.transform.position, Vector3.forward, 1.5f, 256))
            {
                return;
            }

            canMove = false;
            transform.DORotate(new Vector3(0, -90, 0), 0.2f);
            transform.DOJump(transform.position + Vector3.forward * skip, 1f, 1, 0.2f).OnComplete(() => canMove = true);
            onJump.Invoke();
        }
        public void MoveRight()
        {
            if (!canMove)
            {
                return;
            }
            if (Physics.Raycast(this.transform.position, Vector3.back, 1.5f, 256))
            {
                return;
            }
            canMove = false;
            transform.DORotate(new Vector3(0, 90, 0), 0.2f);
            transform.DOJump(transform.position + Vector3.back * skip, 1f, 1, 0.2f).OnComplete(() => canMove = true);
            onJump.Invoke();
        }

        public void OnDieAddListener(UnityAction callback, UnityAction soundCallBack)
        {
            onDie = callback;
            onDieSound = soundCallBack;
        }

        public void OnJumpAddListener(UnityAction callback)
        {
            onJump = callback;
        }


        private void OnTriggerEnter(Collider other)
        {
            if (isDead)
            {
                return;
            }

            if (other.CompareTag("Wall"))
            {
                Debug.Log("Death WALL!!!!!!!!!!!");
                onDie.Invoke();
            }
            else if (other.CompareTag("Car"))
            {
                //transform.DOScaleY(0.01f, 0.2f); okey ale wysokosc z dupy
                if (Mathf.Abs(transform.position.y - startPos.y) < 0.01f)
                {
                    transform.DOScale(deadScale.transform.localScale, 0.1f);
                    transform.DOMove(deadScale.transform.position, 0.1f);

                }
                else
                {

                    transform.DOScaleX(0.01f, duration);
                    transform.SetParent(other.transform);
                }
                onDie.Invoke();
                onDieSound.Invoke();
                isDead = true;


            }
        }


    }
}
